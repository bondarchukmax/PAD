var http = require('http');
var querystring = require('querystring');

exports.postData = function (hostname, port, data ) {
    var postData = querystring.stringify({
        msg: JSON.stringify(data)
    });

    var options = {
        hostname: hostname.toString(),
        port: Number(port),
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': postData.length
        }
    };

    var req = http.request(options, function (res) {
        // console.log('STATUS:', res.statusCode);
        // console.log('HEADERS:', JSON.stringify(res.headers));

        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            // console.log('BODY:', chunk);
        });

        res.on('end', function () {
            // console.log('No more data in response.');
        });
    });

    req.on('error', function (e) {
        console.log('Problem with request:', e.message);
    });

    req.write(postData);
    req.end();

};

