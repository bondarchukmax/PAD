var http = require('http');
var querystring = require('querystring');


exports.createServer = function (port) {
    var server = http.createServer().listen(Number(port));

    server.on('request', function (req, res) {
        if (req.method == 'POST') {
            var body = '';
        }

        req.on('data', function (data) {
            body += data;
        });

        req.on('end', function () {
            var post = querystring.parse(body);
            console.log('localhost:' , port.toString(), '-', body);
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Succes!');
        });
    });

    console.log('Listening on port ', port.toString());
};
