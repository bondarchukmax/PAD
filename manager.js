exports.manager = function () {
    var http = require('http');
    var querystring = require('querystring');
    var postdata = require('./src/postdata');
    var randomChoosePort = require('./src/randomChoose');



    var server = http.createServer().listen(5555);

    server.on('request', function (req, res) {
        if (req.method == 'POST') {
            var body = '';
        }

        req.on('data', function (data) {
            body += data;
        });

        req.on('end', function () {
            var post = querystring.parse(body);
            postdata.postData('localhost', randomChoosePort.randomChoosePort(), body);
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end('Success!');
        });
    });
};

